#!/usr/bin/python

import sys
import urllib.request as request_processor
import urllib.parse as url_parser

from re import compile
from core.connector import Connector
from config import *
from core.scrappy import Scrappy
from core.compilers import Compiler
from textwrap import TextWrapper as Processor
from core.compiler_adapters import ProcessorAdapter
from core.saver import FileSaver


if len(sys.argv) > 2 or len(sys.argv) < 2:
    exit("Функция принимает 1 параметр (корректную ссылку) получено {}".format(len(sys.argv) - 1))

try:
    # Получаем ссылку из аргументов коммандной строки
    url = sys.argv[1]

    # Проверяем правильность ссылки
    valid_url = compile(r'https?://(w{3}\.)?')
    assert valid_url.match(url), 'Неверный формат ссылки. Используйте формат "http://..."'

    # Создаем путь и имя файла для сохранения из полученной ссылки
    dirs, filename = FileSaver.make_path(valid_url.sub('', url))
    params = str(dirs[0]).replace('.', '_')

    # Инициализируем коннектор для получения данных по ссылке
    connector = Connector(request_processor, url_parser)

    # Инициализируем компилятор текста и процессор для обработки полученной от сервера информации
    # Процессор предварительно пропускается через адаптер, чтобы получить необходимые методы
    # и пройти проверку в компиляторе
    try:
        processor = ProcessorAdapter(Processor(**processor_params).fill)
    except NameError:
        processor = None

    compiler = Compiler.get_instance(dirs[0], processor)

    # Создаем парсер
    parser = Scrappy(connector.get_response(url), parser_params[params])

    # Получаем скомпилированную строку от парсера
    text = parser.compile(compiler)

    # Конвертируем строку в байты для корректного сохранения и сохраняем файл с текcтом
    FileSaver.save(dirs, filename, bytearray(text, 'utf-8'))
except BaseException as e:
    exit(e)
