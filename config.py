
# Параметры настройки парсера
# allowed: тэги, которые допустимы для парсинга
# content: тэги, в которых может находится основной контент страницы
# restricted: тэги, которые не нужно включать в парсинг

parser_params = {
    'gazeta_ru': {
        "allowed": ['p', 'h1', 'h2', 'h3', 'b', 'i', 'br', 'span'],
        "content": ['article'],
        "restricted": ['script', 'popup'],
    },
    'lenta_ru': {
        "allowed": ['p', 'h1', 'h2', 'h3', 'b', 'header', 'i', 'br'],
        "content": ['b-topic__content'],
        "restricted": ['script', 'popup'],
    },
    'news_sevas_com': {
        "allowed": ['p', 'h1', 'h2', 'h3', 'b', 'i', 'br'],
        "content": ['news-item'],
        "restricted": ['script', 'popup', 'b-other-news'],
    }
}

processor_params = {
    'width': 80,
    'replace_whitespace': False,
    'tabsize': 0
}
