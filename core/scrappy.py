from html.parser import HTMLParser
import re


class Scrappy(HTMLParser):

    _data = []
    _record_active = 0
    _current_tag = None
    _tag_parent = None
    _link = None

    def __init__(self, raw_data, tags_params):
        """

        :param raw_data:
        :param tags_params:
        """
        super().__init__()                                  # Необходимо создать экземпляр родительского класса
        self._allowed_tags = tags_params['allowed']
        self._content_tags = tags_params['content']
        self._restricted_tags = tags_params['restricted']

        self.feed(raw_data)
        self.close()

    def handle_starttag(self, tag, attrs):
        """
        :param tag: Название тэга текущего элемента
        :param attrs: Список аттрибутов тэга
        :return: void
        """
        if attrs:
            switch = self.find_content(attrs)
            self.record_switch(switch)

        if self._record_active:
            self._current_tag = tag
            self.find_links(attrs)

    def record_switch(self, position: int):

        """
        Позволяет начать сохранение объектов на странице

        :return: void
        """
        self._record_active = position

    def find_content(self, attrs: list) -> bool:

        """
        Ищет ключевые слова указанные в списках content_tags и restricted_tags.
        Если в атрибутах класса тэга присутствуют разрешенные ключи и нет запрещенных,
        то считается, что данный тэг содержит основную информацию статьи

        :param attrs: list
        :return: bool
        """
        for name, value in attrs:
            if value:
                if self._record_active and not self.has_restricted(value):
                    return True
                else:
                    gate = self.has_allowed(value) and not self.has_restricted(value)
                    return gate

    def has_restricted(self, tag_value):
        restricted = sum([tag_value.find(cont_tag) for cont_tag in self._restricted_tags]) \
                     != -len(self._restricted_tags)
        return restricted

    def has_allowed(self, tag_value):
        allowed = sum([tag_value.find(cont_tag) for cont_tag in self._content_tags]) > -1
        return allowed

    def find_links(self, attrs: list):
        """
        Ищет в строке ссылки

        :param attrs:
        :return:
        """
        for name, value in attrs:
            self._link = value if name == 'href' else self._link

    def handle_data(self, data):
        if self.tag_allowed() or self.has_allowed_parent():
            clear_tags = re.compile(r'\n')
            data = clear_tags.sub('', data)
            data = data.replace(u'\xa0', u' ').strip(' ')
            if data:
                self._data.append({'tag': self._current_tag, 'text': data, 'link': self._link})
            self._link = None
        self._tag_parent, self._current_tag = self._current_tag, self._tag_parent

    def tag_allowed(self):
        """
        Проверяет есть ли текущий тэг в списке разрешенных
        :return:
        """
        return self._current_tag in self._allowed_tags

    def has_allowed_parent(self):
        """
        Проверяет есть ли текущий родительский тэг в списке разрешенных
        :return:
        """
        return self._tag_parent in self._allowed_tags

    def get_data(self):
        return self._data

    def compile(self, compiler: object):
        """
        Делигирует компиляцию собранных данных полученному в качестве параметра экземпляру класса Компилятор
        :param compiler: object Compiler
        :return: str возвращает обработанный текст в виде единой строки
        """
        assert callable(getattr(compiler, 'process', None)), 'Компилятор текста имеет неверный формат.'
        try:
            return str(compiler.process(self.get_data()))
        except:
            raise BaseException("Ой! Что-то пошло не так...\nНе удалось скомпилировать текст статьи.")
