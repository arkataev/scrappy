import abc


class Compiler:

    """
    Базовый абстрактный класс компилятора
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, processor):
        """

        :param processor:
        """
        self.processor = processor
        self.compiled_data = ''

    @abc.abstractclassmethod
    def _compile(self, data):
        """
        Базовый метод для компиляции полученных данных в текстовый формат
        Определяет форматирование текста
        :param data: list Список данных для компилирования
        :return: string Возвращает скомпилированный по шаблону текст
        """

    def process(self, data):
        """
        Метод для обработки текста.
        Делигирует функции переданному в качестве параметра экземпляру класса ProcessorAdapter
        :param data: list данные полученные от сервера
        :return: str обработанный текст
        """
        try:
            if self.processor:
                assert callable(getattr(self.processor, 'process', None)), 'Процессор текста имеет неверный формат.'
            return self.processor.process(self._compile(data)) if self.processor else self._compile(data)
        except:
            raise BaseException("Ой! Что-то пошло не так...\nНе удалось обработать текст статьи.")

    @classmethod
    def get_instance(cls, instance, processor):
        """
        Создает экземпляр класса Compiler в зависимости от значения переданного параметром instance
        :param instance: str название сайта для которого нужно создать экземпляр класса (например gazeta.ru)
        :param processor: object экземпляр класса Processor (по умолчанию TextWrapper)
        :return: Compiler object
        """
        # Проверяем обладает ли объект процессора нужными методами
        self_instance = instance.replace('.', '_').split('_')[0].capitalize() + 'Compiler'
        subclasses = {inst.__name__: inst for inst in cls.__subclasses__()}
        assert self_instance in subclasses, 'Компилятор для {} не найден. Для правильной работы,' \
                                            ' создайте подкласс {}'.format(instance, self_instance)
        obj = subclasses[self_instance]
        return obj(processor)


class LentaCompiler(Compiler):

    """
    Компилятор для сайта https://lenta.ru/
    """

    def _compile(self, data):
        for record in data:
            if record['tag'].find('h') > -1:
                self.compiled_data = self.compiled_data.rstrip("\n") + "\n\n" + "### " + record['text'] + "\n\n"
            if record['link']:
                self.compiled_data = self.compiled_data.rstrip("\n") + " " \
                                     + record['text'] + " ['" + record['link'] + "']"
            if record['tag'] == 'p':
                self.compiled_data = self.compiled_data.rstrip("\n") + "\n\n" + record['text']
            if record['tag'] in ['i', 'b', 'strong']:
                self.compiled_data += "\n\n" + "# " + record['text'] + "\n\n"

        return str(self.compiled_data)


class GazetaCompiler(Compiler):

    """
    Компилятор для сайта http://www.gazeta.ru/
    """
    def _compile(self, data):
        for record in data:
            if record['tag'].find('h') > -1:
                self.compiled_data = self.compiled_data.rstrip("\n") + "\n\n" + "### " + record['text'] + "\n\n"
            if record['link']:
                self.compiled_data = self.compiled_data.rstrip("\n") + " " \
                                     + record['text'] + " ['" + record['link'] + "']"
            if record['tag'] in ['p', 'span']:
                self.compiled_data = self.compiled_data.rstrip("\n") + "\n\n" + record['text']
            if record['tag'] in 'ib':
                self.compiled_data += record['text']

        return str(self.compiled_data)

    class NewsCompiler(Compiler):
        """
        Компилятор для сайта http://news.sevas.com/
        """
        def _compile(self, data):
            p_count = 0
            previous_tag = ''
            for record in data:
                if record['tag'].find('h') > -1:
                    self.compiled_data += "### " + record['text'] + "\n\n"
                if record['link']:
                    previous_tag = 'a'
                    # Дублируется текст если ссылка есть заголовке
                    self.compiled_data += " " + record['text'] + " ['" + record['link'] + "']"
                if record['tag'] == 'p':
                    if p_count > 3 and previous_tag != 'a':
                        self.compiled_data += "\n\n"
                        p_count = 0
                    self.compiled_data += record['text']
                    p_count += 1
                    previous_tag = 'p'
                if record['tag'] in ['i', 'b', 'strong', 'code']:
                    self.compiled_data += " " + record['text']

            return str(self.compiled_data)
