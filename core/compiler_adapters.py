class ProcessorAdapter:

    """
    Адаптер для класса Compiler
    """
    def __init__(self, processor_func):
        self.processor = processor_func

    def process(self, text: str):
        return self.processor(text)

