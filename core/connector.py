class Connector(object):

    """
    Базовый класс коннектора
    """
    # TODO:: класс можно сделать абстрактным

    headers = {"User-Agent": "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11",
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
               'Content-type': 'text/html', 'charset': 'utf-8'}

    def __init__(self, request_handler: object, parser: object):
        self.request_handler = request_handler
        self.parser = parser
        self._response = None

    def _connect(self, url: str):
        """

        :param url:
        :return:
        """
        # Проверяем имеет ли полученный обработчик запросов нужные методы
        if callable(getattr(self.request_handler, 'Request', None)):
            request = self.request_handler.Request(url, headers=self.headers)
        else:
            exit('Неверный формат обработчика запросов')
        # Пробуем открыть полученную ссылку
        if callable(getattr(self.request_handler, 'urlopen', None)):
            try:
                return self.request_handler.urlopen(request)
            except Exception as e:
                print(e.code, e.msg)
                exit("Ошибка : Неудалось получить данные по указанной ссылке")
        else:
            exit('В обработчике запроса отсутствует метод urlopen')

    def get_response(self, url: str) -> str:
        """
        Получает и декодирует ответ от сервера по указанной ссылке
        :param url: str ссылка на ресурс
        :return data: str декодированный ответ от сервера
        """
        try:
            self._response = self._connect(url)
        except ValueError:
            exit('Необходимо указать верную ссылку на страницу с данными')

        if self._response:
            info = self._response.info()
            try:
                # Пробуем найти сведения о кодировке в данных полученных от сервера
                encoding = dict(info)['Content-Type'].split('=')[1]
            except IndexError:
                # Если сервер не сообщает кодировку страницы, используем utf-8 по умолчанию
                encoding = 'utf-8'
            # Читаем данные из ответа сервера
            data = self._response.read()
            # Пробуем декодировать полученные данные в указанной кодировке
            try:
                data = data.decode(encoding)
            except UnicodeDecodeError:
                # Если кодировка не подходит пробуем использовать кириллическую windows-1251
                try:
                    data = data.decode('windows-1251')
                except UnicodeDecodeError as e:
                    print(e.code, e.msg)
                    exit('Не удалось определить кодировку страницы')
        else:
            exit('Не удалось получить ответ от сервера')

        return data
