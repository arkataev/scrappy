import os


class FileSaver(object):

    @classmethod
    def make_path(cls, path_str):
        path = [i for i in path_str.split('/') if i is not '']
        # Дерево каталогов
        dir_tree = path[:-1]
        # Имя файла
        file_name = str(path[len(dir_tree):][0])
        assert len(dir_tree) > 0, "Неудалось построить дерево каталогов для сохранения файла. " \
                                  "Используйте полный url для выбранной статьи"
        return dir_tree, file_name

    @classmethod
    def save(cls, dirs, filename, string):
        try:
            path = os.path.join(*dirs)
        except:
            exit("Неудалось создать каталоги для хранения файла. Возможно указан не верный формат ссылки")
        # Создаем дерево каталогов для хранения файла
        try:
            os.makedirs(path)
        except FileExistsError:
            # Если каталог уже существует продолжаем
            pass
        # Пробуем сохранить данные в файл по указанному пути
        try:
            with open(path + "\\" + filename + '.txt', 'wb') as file:
                file.write(string)
                print('#### Информация успешно сохранена ####')
        except Exception as e:
            exit('Не удалось сохранить файл:(')

